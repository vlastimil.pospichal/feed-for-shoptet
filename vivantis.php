<?php
/**
 * @author     Ing. Vlastimil Pospíchal <vlastimil.pospichal@gmail.com>
 * @copyright  (c) 2018
 */

function readFeed($filename) {
	$channel = curl_init($filename);
	curl_setopt_array($channel, array(
		CURLOPT_RETURNTRANSFER  =>true,
	));
	$feed = curl_exec($channel);
	curl_close($channel);
	return $feed;
}

ini_set('default_socket_timeout', 180);
$config = parse_ini_file(".htconfig.ini");
$feed = readFeed($config["url"]);
header('Content-Type: text/xml;charset=UTF-8');
$doc = new DOMDocument();
@$doc->loadXML($feed);
$xsl = new XSLTProcessor();
$xsl->importStyleSheet(DOMDocument::load($config["xslFileName"]));
echo $xsl->transformToXML($doc);
