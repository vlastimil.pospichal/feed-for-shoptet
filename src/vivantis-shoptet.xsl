<?xml version="1.0"?>
<!--
	 Copyright (c) 2018 Ing. Vlastimil Pospíchal <vlastimil.pospichal@gmail.com>
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
	<xsl:strip-space elements="description main alt name value"/>
	<xsl:output method="xml"
		encoding="UTF-8"
		indent="yes"
		cdata-section-elements="a"
		/> 

	<xsl:template match="/shop">
		<SHOP><xsl:apply-templates select="products/product"/></SHOP>
	</xsl:template>

	<xsl:template match="products/product">
		<xsl:if test="position()&gt;0">
		<SHOPITEM>
			<xsl:apply-templates select="id"/>
			<xsl:apply-templates select="name"/>
			<xsl:apply-templates select="description"/>
			<xsl:apply-templates select="manufacturer"/>
			<xsl:apply-templates select="parameters"/>
			<xsl:apply-templates select="inStock"/>
			<xsl:apply-templates select="categories"/>
			<xsl:apply-templates select="images"/>
			<xsl:apply-templates select="prices"/>
			<xsl:apply-templates select="eans"/>
		</SHOPITEM>
	</xsl:if>
</xsl:template>

<xsl:template match="id">
	<CODE><xsl:value-of select="."/></CODE>
</xsl:template>

<xsl:template match="name">
	<NAME><xsl:value-of select="."/></NAME>
</xsl:template>

<xsl:template match="description">
	<ITEM_TYPE>product</ITEM_TYPE>
	<xsl:choose>
		<xsl:when test="string-length(.)&gt;0">
			<SHORT_DESCRIPTION><xsl:value-of select="."/></SHORT_DESCRIPTION>
			<DESCRIPTION><xsl:value-of select="."/></DESCRIPTION>
		</xsl:when>
		<xsl:otherwise>
			<SHORT_DESCRIPTION><xsl:value-of select="../name"/></SHORT_DESCRIPTION>
			<DESCRIPTION><xsl:value-of select="../name"/></DESCRIPTION>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="manufacturer">
	<xsl:if test="string-length(.)!=0" >
		<MANUFACTURER><xsl:value-of select="."/></MANUFACTURER>
	</xsl:if>
</xsl:template>

<xsl:template match="parameters">
	<xsl:variable name="gender" select="../gender"/>
	<xsl:if test="string-length(parameter[name='Záruka']/value)!=0" >
		<WARRANTY><xsl:value-of select="parameter[name='Záruka']/value"/></WARRANTY>
	</xsl:if>
	<TEXT_PROPERTIES>
		<xsl:apply-templates select="parameter" mode="parameters"/>
		<xsl:choose>
			<xsl:when test="$gender='woman'">
				<TEXT_PROPERTY>
					<NAME>Vhodné pro</NAME>
					<VALUE>Ženy</VALUE>
				</TEXT_PROPERTY>
			</xsl:when>
			<xsl:when test="$gender='man'">
				<TEXT_PROPERTY>
					<NAME>Vhodné pro</NAME>
					<VALUE>Muže</VALUE>
				</TEXT_PROPERTY>
			</xsl:when>
			<xsl:when test="$gender='child'">
				<TEXT_PROPERTY>
					<NAME>Vhodné pro</NAME>
					<VALUE>Děti</VALUE>
				</TEXT_PROPERTY>
			</xsl:when>
		</xsl:choose>
	</TEXT_PROPERTIES>
</xsl:template>

<xsl:template match="parameter" mode="parameters">
	<TEXT_PROPERTY>
		<NAME><xsl:value-of select="name"/></NAME>
		<VALUE><xsl:value-of select="value"/></VALUE>
	</TEXT_PROPERTY>
</xsl:template>

<xsl:template match="prices">
	<CURRENCY>CZK</CURRENCY>
	<PRICE><xsl:value-of select="CZK/retailPrice/withoutVat"/></PRICE>
	<PRICE_VAT><xsl:value-of select="CZK/retailPrice/withVat"/></PRICE_VAT>
	<STANDARD_PRICE><xsl:value-of select="CZK/retailPrice/withVat"/></STANDARD_PRICE>
	<PURCHASE_PRICE><xsl:value-of select="CZK/wholesalePrice/withVat"/></PURCHASE_PRICE>
	<VAT><xsl:value-of select="../vat"/></VAT>
	<xsl:if test="CZK/wholesalePrice/withVat&gt;=2500">
		<APPENDIX>+ Doprava ZDARMA#GIFTS# Dárkový kupón v hodnotě 200 Kč</APPENDIX>
		<FREE_SHIPPING>1</FREE_SHIPPING>
	</xsl:if>
</xsl:template>

<xsl:template match="categories">
	<xsl:variable name="gender" select="../gender"/>
	<CATEGORIES>
		<xsl:choose>
			<xsl:when test="$gender='woman'">
				<CATEGORY>Dámské hodinky</CATEGORY>
			</xsl:when>
			<xsl:when test="$gender='man'">
				<CATEGORY>Pánské hodinky</CATEGORY>
			</xsl:when>
			<xsl:when test="$gender='child'">
				<CATEGORY>Dětské hodinky</CATEGORY>
			</xsl:when>
		</xsl:choose>
		<xsl:apply-templates select="id" mode="category"/>
	</CATEGORIES>
</xsl:template>

<xsl:template match="id" mode="category">
	<xsl:variable name="id" select="."/>
	<xsl:variable name="name" select="/shop/categories/category[id=$id]/name"/>
	<xsl:choose>
		<xsl:when test="$name='Hodiny'">
			<!-- Kategorii Hodiny ignorujeme -->
		</xsl:when>
		<xsl:when test="$name='Hodinky'">
			<!-- Kategorii Hodinky ignorujeme -->
		</xsl:when>
		<xsl:otherwise>
			<CATEGORY><xsl:value-of select="$name"/></CATEGORY>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="images">
	<IMAGES>
		<xsl:for-each select="main|alt">
			<xsl:if test="string-length(.)!=0" >
				<IMAGE><xsl:value-of select="."/></IMAGE>
			</xsl:if>
		</xsl:for-each>
	</IMAGES>
</xsl:template>

<xsl:template match="inStock">
	<xsl:variable name="amount" select="."/>
	<STOCK>
		<AMOUNT><xsl:value-of select="."/></AMOUNT>
		<MINIMAL_AMOUNT>1</MINIMAL_AMOUNT>
	</STOCK>
	<UNIT>ks</UNIT>
	<xsl:choose>
		<xsl:when test="$amount&gt;0">
			<AVAILABILITY_IN_STOCK>Skladem</AVAILABILITY_IN_STOCK>
		</xsl:when>
		<xsl:otherwise>
			<AVAILABILITY_OUT_OF_STOCK>Na dotaz</AVAILABILITY_OUT_OF_STOCK>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="eans">
	<xsl:choose>
		<xsl:when test="string-length(main)!=0">
			<EAN><xsl:value-of select="substring(main,1,13)"/></EAN>
		</xsl:when>
		<xsl:otherwise>
			<EAN>000<xsl:value-of select="../id"/></EAN>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>
</xsl:stylesheet>
