Transformace feedu z b2b.vivantis.cz do Shoptetu

### Použití
Shoptet -> Produkty -> Automatické importy -> Přidat import

Vyplnit:
- Jméno: Název dodavatele
- Import kód: Vlastní označení importu
- Typ dodavatele: Shoptet
- URL adresa XML feedu: http://server/vivantis.php
